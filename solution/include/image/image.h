#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#pragma pack(push, 1)
struct pixel {
	uint8_t r, g, b;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(uint16_t width, uint16_t height);
void empty_image(struct image* image);
uint32_t global_offset(uint64_t height);
#endif
