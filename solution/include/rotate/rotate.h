#ifndef ROTATE_H
#define ROTATE_H

#include "../file_functions/f_functions.h"
#include "../image/image.h"

enum operation_status rotate(struct image* img);

#endif
