#ifndef F_FUNCTIONS_H
#define F_FUNCTIONS_H

#include "../image/image.h"
#include <stdio.h>

enum operation_status {
    SUCCESSFULLY = 0,
    OPEN_ERROR,
    READ_ERROR,
    CLOSE_ERROR,
    ALLOCATION_ERROR
};

enum operation_status open_file(FILE **f_in, const char *const f_name, const char *const chmod);

enum operation_status close_file(FILE *f_in);

#endif
