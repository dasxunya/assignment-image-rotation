#ifndef BMP_H
#define BMP_H

#include "../image/image.h"
#include  <stdint.h>
#include <stdio.h>

typedef struct __attribute__((packed)) {
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
} bmp_header;

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_PIXEL_ERROR
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE *f_in, struct image * const img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp(FILE *f_out, struct image *const img);

enum read_status read_bmp_header(FILE *f_in, bmp_header *const header);

enum write_status write_bmp_header(FILE *f_out, uint64_t width, uint64_t height);

#endif
