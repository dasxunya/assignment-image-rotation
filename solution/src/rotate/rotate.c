#include "../../include/rotate/rotate.h"
#include <inttypes.h>

enum operation_status rotate(struct image *img) {
    struct image new_image = create_image(img->height, img->width);
        struct pixel my_pixel;
        for (size_t i = 0; i < img->height; i++) {
            for (size_t j = 0; j < img->width; j++) {
                my_pixel = img->data[img->width * i + j];
                new_image.data[new_image.width * j + img->height - 1 - i] = my_pixel;
            }
        }
        *img = new_image;
        return SUCCESSFULLY;
}
