#include "../../include/file_functions/f_functions.h"
enum operation_status open_file(FILE** f_in, const char* const f_name, const char* const chmod){
        *f_in = fopen(f_name, chmod);
        if(*f_in == NULL){return OPEN_ERROR;}
        return SUCCESSFULLY;
}
enum operation_status close_file(FILE* f_in){
    int64_t response = fclose(f_in);
    if(response == 0){return SUCCESSFULLY;}
    return CLOSE_ERROR;
}
