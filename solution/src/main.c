#include "../include/bmp/bmp.h"
#include "../include/file_functions/f_functions.h"
#include "../include/rotate/rotate.h"

int main(int argc, char **argv) {

    if (argc < 3) {
        printf("Введите больше аргументов\n");
        return 0;
    }
    FILE *f_in = NULL;
    FILE *f_out = NULL;
    struct image img = {0};

    switch (open_file(&f_in, argv[1], "rb")) {
         case OPEN_ERROR:
            fprintf(stderr,"Ошибка при открытии файла\n");
            return 1;
        case SUCCESSFULLY:
            printf("Файл открылся\n");
            break;
        default:
        	break;
    }

    switch (from_bmp(f_in, &img)) {
        case READ_OK:
            printf("Картинка считана\n");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Ошибка при считывании заголовка\n");
            return 1;
        case  READ_PIXEL_ERROR:
            fprintf(stderr, "Ошибка при считывании пикселей\n");
            return 1;
            default:
            break;
    }


    switch (close_file(f_in)) {
        case SUCCESSFULLY:
            printf("Файл закрылся\n");
            break;
        case CLOSE_ERROR:
            fprintf(stderr, "Ошибка при закрытии файла\n");
            return 1;
            default:
            break;
    }

    switch (rotate(&img)) {
        case ALLOCATION_ERROR:
            fprintf(stderr, "Ошибка с выделением памяти\n");
            return 1;
        case SUCCESSFULLY:
            printf("Изображение повернуто\n");
            break;
            default:
            break;
    }


    switch (open_file(&f_out, argv[2], "wb")) {
        case OPEN_ERROR:
            fprintf(stderr, "Ошибка при открытии файла\n");
            return 1;
        case SUCCESSFULLY:
            printf("Файл открылся\n");
            break;
            default:
            break;
    }

    switch (to_bmp(f_out, &img)) {
        case WRITE_ERROR:
            fprintf(stderr, "Ошибка при записи заголовков\n");
            return 1;
        case WRITE_OK:
            printf("Заголовки записаны\n");
            break;
            default:
            break;
    }

    switch (close_file(f_out)) {
        case SUCCESSFULLY:
            printf("Файл закрылся\n");
            break;
        case CLOSE_ERROR:
            fprintf(stderr, "Ошибка при закрытии файла\n");
            return 1;
            default:
            break;
    }

    empty_image(&img);

    printf("Конец\n");

    return 0;
}
