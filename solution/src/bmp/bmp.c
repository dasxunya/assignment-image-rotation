#include "../../include/bmp/bmp.h"
#include "../../include/file_functions/f_functions.h"
#include <inttypes.h>
#include <stdlib.h>

//FOR_READING
enum read_status read_bmp_header(FILE *f_in, bmp_header *const header) {
    if (fread(header, sizeof(bmp_header), 1, f_in) != 1) { return READ_INVALID_HEADER; }
    return READ_OK;
}



enum read_status from_bmp(FILE *f_in, struct image * img) {
    bmp_header my_header = {0};
    struct pixel pixel;
    uint8_t offset;
    

    enum read_status bmp_status = read_bmp_header(f_in, &my_header);
    if (bmp_status != SUCCESSFULLY) return bmp_status;

    const uint32_t height = my_header.biHeight;
    const uint32_t width = my_header.biWidth;
    offset = (3*my_header.biWidth) % 4;
    if (offset != 0) {
        offset = 4 - offset;
    }
	
img->height = height;
img->width = width;
    img->data = malloc(width * height * sizeof(struct pixel));
    for (size_t i = 0; i < my_header.biHeight; i++){
        for (size_t j = 0; j < my_header.biWidth; j++) {
            fread(&pixel, sizeof(struct pixel), 1, f_in);
            img->data[my_header.biWidth * i + j] = pixel;
        }

        if (offset != 0){
            fread(&pixel, sizeof(unsigned char)*offset, 1, f_in);
        }
    }

    return READ_OK;
}

//FOR_WRITING

enum write_status write_bmp_header(FILE *f_out, uint64_t width, uint64_t height) {
    bmp_header header = {0};
    header.bfType = 0x4D42;
	header.bfReserved = 0;
	header.bOffBits = 54;
	header.biSize = 40;
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = 1;
	header.biBitCount = 24;
	header.biCompression = 0;
	header.biSizeImage = width * height * 3;
	header.bfileSize = header.biSizeImage + sizeof(bmp_header);
	header.biXPelsPerMeter = 0;
	header.biYPelsPerMeter = 0;
	header.biClrUsed = 0;
	header.biClrImportant = 0;

    if(fwrite(&header, sizeof(bmp_header), 1, f_out)) {
        return WRITE_OK;
    }
    return WRITE_ERROR;
}

enum write_status to_bmp(FILE *f_out, struct image *const img) {
size_t padding = (4 - (img->width * sizeof(struct pixel) % 4)) % 4;
    bmp_header header = {0};
    struct pixel pixel;

    header.bfType = 0x4D42;
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header.bfileSize = header.biSizeImage + sizeof(header);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    uint8_t offset = (4 - (header.biWidth * sizeof(struct pixel) % 4)) % 4;

    fwrite(&header, sizeof(bmp_header), 1, f_out);
    fseek(f_out, header.bOffBits, SEEK_SET);

    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            pixel = img->data[img->width * i + j];
            fwrite(&pixel, sizeof(struct pixel), 1, f_out);
        }
        if (offset != 0) {
            for (size_t j = 0; j < offset; j++) {
                pixel.b = 0;
                pixel.r = 0;
                pixel.g = 0;
                fwrite(&pixel, sizeof(unsigned char), 1, f_out);
            }
        }
    }
    /*
    if (!f_out) {
        return WRITE_ERROR;
    }
    if (status != WRITE_OK)
        return status;
    */
    return WRITE_OK;

}
