#include "../../include/image/image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>

struct image create_image(uint16_t width, uint16_t height) {
    struct image img = {0};
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

uint32_t global_offset(uint64_t height) { return (4 - ((3 * height) % 4)) % 4; }
void empty_image(struct image* image){
    free(image->data);
}

